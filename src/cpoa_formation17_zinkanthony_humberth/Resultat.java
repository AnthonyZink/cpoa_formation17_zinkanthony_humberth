package cpoa_formation17_zinkanthony_humberth;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class Resultat extends HashMap<Matiere, ArrayList<Double>>{



	public Resultat(){
		
	}

	public void afficher(){
		Set<Matiere> key = this.keySet();
		for (Matiere m : key) {
			System.out.println(m + " " + this.get(m));

		}
	}
	
	public void addNote(Matiere m, double n){
		
		if(this.containsKey(m)){
			this.get(m).add(n);
		}
		else{
			this.put(m, new ArrayList<Double>());
			this.get(m).add(n);
		}
		
		
	}
	

}
