package cpoa_formation17_zinkanthony_humberth;

public class Matiere {
	
	private String nom;
	private int coefficient;
	
	public Matiere(String n, int c){
		this.nom = n;
		this.coefficient = c;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}

	@Override
	public String toString() {
		return "Matiere [nom=" + nom + ", coefficient=" + coefficient + "]";
	}

	
}
