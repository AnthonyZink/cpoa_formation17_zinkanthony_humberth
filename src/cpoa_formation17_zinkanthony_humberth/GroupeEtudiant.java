package cpoa_formation17_zinkanthony_humberth;

import java.util.ArrayList;
import java.util.HashMap;

public class GroupeEtudiant {

	ArrayList<Etudiant> listEtu;
	
	
	public GroupeEtudiant(){
		
		listEtu = new ArrayList<Etudiant>();
		
	}
	
	public void ajouterEtudiant(Etudiant etudiant){
		
		listEtu.add(etudiant);
	}
	
	public void supprimerEtudiant(Etudiant etudiant){
		
		listEtu.remove(etudiant);
	}
	
	public double calculerMoyenneGroupeMatiere(Matiere m){
		
		double somme = 0;
		int count = 0;
		double moy = 0;
		
		for(int i = 0; i< listEtu.size(); i++){
			somme = somme + listEtu.get(i).calculerMoyenneMatiere(m);
			count++;
		}
		
		moy = somme/count;
		return moy;
	}
	
	public double calculerMoyenneGroupe(){
		
		double somme = 0;
		int count = 0;
		double moy = 0;
		
		for(int i = 0; i<listEtu.size(); i++){
			
			somme = somme + listEtu.get(i).calculerMoyenneGeneral();
			count++;
		}
		
		moy = somme/count;
		
		return moy;
	}
	
}
