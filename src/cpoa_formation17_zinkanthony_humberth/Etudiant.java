package cpoa_formation17_zinkanthony_humberth;

import java.util.Set;

public class Etudiant {

	Identite id;
	GroupeEtudiant groupe;
	Resultat resultat;
	Formation formation;


	public Etudiant(Identite id, GroupeEtudiant grp, Formation f ,Resultat res){

		this.id = id;
		this.groupe = grp;
		this.resultat = res;
		this.formation = f;
	}

	public void ajouterNote(Matiere m, double note){

		if((note <= 20) && (note >= 0)){
			resultat.addNote(m,note);
		}
	}

	public double calculerMoyenneMatiere(Matiere m){

		double moy = 0;

		if(resultat.containsKey(m)){

			double somme = 0;
			int count = 0;


			for (int i = 0; i < resultat.get(m).size(); i++) {

				somme = somme + resultat.get(m).get(i);
				count++;
			}

			moy = somme/count;
		}

		return moy;
	}
	
	public double calculerMoyenneGeneral(){
		
		Set<Matiere> m = resultat.keySet();
		
		double somme = 0;
		double moy = 0;
		int count = 0;
		
		for(Matiere mat : m){
			
			somme = somme + (calculerMoyenneMatiere(mat) * mat.getCoefficient() );
			count = count + mat.getCoefficient();	
		}
		
		moy = somme/count;
		
		return moy;
	}



}
