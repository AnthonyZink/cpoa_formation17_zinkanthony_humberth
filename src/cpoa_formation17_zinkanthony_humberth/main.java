package cpoa_formation17_zinkanthony_humberth;

public class main {

	public static void main(String[] args) {
		Matiere m = new Matiere("maths", 3);
		Matiere m2 = new Matiere("lol", 2);
		
		Resultat r = new Resultat();
		
		Etudiant etu;
		
		Identite id1 = new Identite("idu1","Albert","Bernard");
		GroupeEtudiant groupe1 = new GroupeEtudiant();
		Formation formation1 = new Formation("chezPas");
		
		etu = new Etudiant(id1,groupe1,formation1,r);
		etu.ajouterNote(m, 20);
		etu.ajouterNote(m2, 10);
		
		
		System.out.println(etu.calculerMoyenneMatiere(m2));
		System.out.println(etu.calculerMoyenneGeneral());
	}

}
