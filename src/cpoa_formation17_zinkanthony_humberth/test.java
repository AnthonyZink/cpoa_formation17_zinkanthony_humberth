package cpoa_formation17_zinkanthony_humberth;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class test {
	
	Etudiant e;
	Etudiant e2;
	Resultat r;
	Matiere m;
	Matiere m2;
	Formation f;
	GroupeEtudiant g ;
	

	@Before
	public void before(){
		g = new GroupeEtudiant();
		
		f = new Formation("informatique");
		
		m = new Matiere("maths", 2);
		m2 = new Matiere("anglais", 1);
		
		r = new Resultat();
		r.addNote(m, 10);
		r.addNote(m2, 20);
		
		e = new Etudiant(new Identite("etu1", "toto", "z"), g, f, r);
		e2 = new Etudiant(new Identite("etu1", "toto", "z"), g, f, r);
		
		g.ajouterEtudiant(e);
		g.ajouterEtudiant(e2);
	}
	
	
	@Test
	public void testCalculerMoyenneMatiereEtudiant() {
		double res = e.calculerMoyenneMatiere(m);
		assertEquals(10, res,0.01);
	}
	
	@Test
	public void testCalculerMoyenneGeneralEtudiant() {
		double res = e.calculerMoyenneGeneral();
		assertEquals(13.33, res,0.01);
	}
	
	@Test
	public void testCalculerMoyenneGeneralGroupe() {
		double res = g.calculerMoyenneGroupe();
		assertEquals(13.33, res,0.01);
	}
	
	@Test
	public void testCalculerMoyenneMatiereGroupe() {
		double res = g.calculerMoyenneGroupeMatiere(m2);
		assertEquals(20, res,0.01);
	}

}
